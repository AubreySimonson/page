//put this script on a gameobject with a collider marked as "isTrigger"
//to make controller buzz if controller enters the object
//make sure the controller has a collider of some kind and one of the two objects involved in the collision has a rigidbody

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Management;
using UnityEngine.InputSystem;

public class HapticObject : MonoBehaviour
{
    public float vibrateAmp, vibrateTime;//for haptics intensities. Keep amplitudes less than 1. Time is in seconds.

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<UnityEngine.XR.Interaction.Toolkit.ActionBasedController>() != null)//if the other gameobject has a "this is a controller" script
        {
            UnityEngine.XR.Interaction.Toolkit.ActionBasedController xRController = other.gameObject.GetComponent<UnityEngine.XR.Interaction.Toolkit.ActionBasedController>();
            xRController.SendHapticImpulse(vibrateAmp, vibrateTime);
        }
    }
}
